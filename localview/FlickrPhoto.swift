//
//  FlickrPhoto.swift
//  localview
//
//  Created by Zach Freeman on 8/13/15.
//  Copyright (c) 2015 sparkwing. All rights reserved.
//

import UIKit

public class FlickrPhoto: NSObject {
  
  
  var photoSetId : String?
  var smallImageUrl : NSURL?
  var bigImageUrl : NSURL?
  var title : String?
  
}
