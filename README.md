# LocalView #

This is a simple universal iOS app written in Swift that pulls images from the Flickr API based on the user's current location.

### Included Dependencies ###
* [AlamoFire](https://github.com/Alamofire/Alamofire)
* [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON)
* [SDWebImage](https://github.com/rs/SDWebImage)
* [Quick](https://github.com/Quick/Quick)
* [Nimble](https://github.com/Quick/Nimble)
* [Swinject](https://github.com/Swinject/Swinject)

### Getting Started ###
1. Clone the project
2. cd to the project
3. git submodule init (Quick & Nimble are included via git submodules)
4. git submodule update
5. carthage update (Swinject is included via carthage)
6. open localview.xcodeproj

### Testing ###

Press Command+u to run all the tests at once
